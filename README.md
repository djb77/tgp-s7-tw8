# The Galaxy Project for Samsung Galaxy S7

![TGP Logo](https://gitlab.com/djb77/tgp-s7-tw8/raw/master/tools/logo.jpg)

## Compatible with the following variants
- G930F
- G930FD
- G930W8
- G935F
- G935FD
- G935W8

## How to check for updates via git

Use the "git pull" command to keep the repository up-to-date

## How to build a flashable .zip (Linux only)

Execute the "build.sh" file via Terminal and wait for it to be compressed
