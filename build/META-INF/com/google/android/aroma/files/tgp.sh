#!/sbin/sh
# --------------------------------
# TGP INSTALLER v10.0.15
# tgp.sh portion
#
# Created by @djb77 from XDA
# Credit also goes to @lyapota
# @farovitus, @Morogoku, @dwander,
# @Chainfire, @osm0sis, @Tkkg1994,
# @ambasadii, and @ALEXNDR
# for code snippets.
#
# DO NOT USE ANY PORTION OF THIS
# CODE WITHOUT MY PERMISSION!!
# --------------------------------

# Read option number from updater-script
OPTION=$1
CSC=$2

# Block location
BLOCK=/dev/block/platform/155a0000.ufs/by-name

# Variables
TGPTEMP=/tmp/tgptemp
AROMA=/tmp/aroma
CONFIG=/data/media/0/TGP/config
BUILDPROP=/system/build.prop
CSC_DIRECTORY=/system/csc
FLOATINGFEATURE=/system/etc/floating_feature.xml
KERNEL_REMOVE="init.services.rc init.PRIME-Kernel.rc init.spectrum.sh init.spectrum.rc init.primal.rc init.noto.rc kernelinit.sh wakelock.sh super.sh cortexbrain-tune.sh spectrum.sh kernelinit.sh spa init_d.sh initd.sh moro-init.sh sysinit.sh tgpkernel.sh noto.sh"

if [ $OPTION == "setup" ]; then
	## Set Permissions
	chmod 755 $AROMA/adb
	chmod 755 $AROMA/adb.bin
	chmod 755 $AROMA/fastboot
	chmod 755 $AROMA/busybox
	chmod 755 $AROMA/tar
	chmod 755 $AROMA/zip
	chmod 755 $AROMA/tgp.sh
	exit 10
fi

if [ $OPTION == "config_check" ]; then
	## Config Check
	# If config backup is present, alert installer
	mount $BLOCK/USERDATA /data
	if [ -e $CONFIG/tgp-backup.prop ]; then
		echo "install=1" > /tmp/aroma/backup.prop
	fi
	exit 10
fi

	## Variant Checks for S7
if [ $OPTION == "check_g930f" ]; then
	echo "install=1" > $AROMA/apk-patch.prop
	echo "install=1" > $AROMA/blcp.prop
	echo "install=1" > $AROMA/g930x.prop
	echo "install=1" > $AROMA/g930f.prop
	exit 10
fi

if [ $OPTION == "check_g930fd" ]; then
	echo "install=1" > $AROMA/apk-patch.prop
	echo "install=1" > $AROMA/blcp.prop
	echo "install=1" > $AROMA/g930x.prop
	echo "install=1" > $AROMA/g930fd.prop
	exit 10
fi

if [ $OPTION == "check_g930k" ]; then
	echo "install=1" > $AROMA/apk-patch.prop
	echo "install=1" > $AROMA/g930x.prop
	echo "install=1" > $AROMA/g930k.prop
	exit 10
fi

if [ $OPTION == "check_g930l" ]; then
	echo "install=1" > $AROMA/g930x.prop
	echo "install=1" > $AROMA/g930l.prop
	exit 10
fi

if [ $OPTION == "check_g930s" ]; then
	echo "install=1" > $AROMA/apk-patch.prop
	echo "install=1" > $AROMA/g930x.prop
	echo "install=1" > $AROMA/g930s.prop
	exit 10
fi

if [ $OPTION == "check_g930w8" ]; then
	echo "install=1" > $AROMA/apk-patch.prop
	echo "install=1" > $AROMA/blcp.prop
	echo "install=1" > $AROMA/g930x.prop
	echo "install=1" > $AROMA/g930w8.prop
	exit 10
fi

	## Variant Checks for S7 Edge
if [ $OPTION == "check_g935f" ]; then
	echo "install=1" > $AROMA/blcp.prop
	echo "install=1" > $AROMA/g935x.prop
	echo "install=1" > $AROMA/g935f.prop
	exit 10
fi

if [ $OPTION == "check_g935fd" ]; then
	echo "install=1" > $AROMA/blcp.prop
	echo "install=1" > $AROMA/g935x.prop
	echo "install=1" > $AROMA/g935fd.prop
	exit 10
fi

if [ $OPTION == "check_g935k" ]; then
	echo "install=1" > $AROMA/g935x.prop
	echo "install=1" > $AROMA/g935k.prop
	exit 10
fi

if [ $OPTION == "check_g935l" ]; then
	echo "install=1" > $AROMA/g935x.prop
	echo "install=1" > $AROMA/g935l.prop
	exit 10
fi

if [ $OPTION == "check_g935s" ]; then
	echo "install=1" > $AROMA/g935x.prop
	echo "install=1" > $AROMA/g935s.prop
	exit 10
fi

if [ $OPTION == "check_g935w8" ]; then
	echo "install=1" > $AROMA/blcp.prop
	echo "install=1" > $AROMA/g935x.prop
	echo "install=1" > $AROMA/g935w8.prop
	exit 10
fi

if [ $OPTION == "config_backup" ]; then
	## Backup Config
	# Check if TGP folder exists on Internal Memory, if not, it is created
	if [ ! -d /data/media/0/TGP ]; then
		mkdir /data/media/0/TGP
		chmod 777 /data/media/0/TGP
	fi
	# Check if config folder exists, if it does, delete it 
	if [ -d $CONFIG-backup ]; then
		rm -rf $CONFIG-backup
	fi
	# Check if config folder exists, if it does, ranme to backup
	if [ -d $CONFIG ]; then
		mv -f $CONFIG $CONFIG-backup
	fi
	# Check if config folder exists, if not, it is created
	if [ ! -d $CONFIG ]; then
		mkdir $CONFIG
		chmod 777 $CONFIG
	fi
	# Copy files from $AROMA to backup location
	cp -f $AROMA/* $CONFIG
	# Delete any files from backup that are not .prop files
	find $CONFIG -type f ! -iname "*.prop" -delete
	# Remove unwanted .prop files from the backup
	cd $CONFIG
	[ -f "$CONFIG/g930f.prop" ] && rm -f $CONFIG/g930f.prop
	[ -f "$CONFIG/g930fd.prop" ] && rm -f $CONFIG/g930fd.prop
	[ -f "$CONFIG/g930k.prop" ] && rm -f $CONFIG/g930k.prop
	[ -f "$CONFIG/g930l.prop" ] && rm -f $CONFIG/g930l.prop
	[ -f "$CONFIG/g930s.prop" ] && rm -f $CONFIG/g930s.prop
	[ -f "$CONFIG/g930w8.prop" ] && rm -f $CONFIG/g930w8.prop
	[ -f "$CONFIG/g930x.prop" ] && rm -f $CONFIG/g930x.prop
	[ -f "$CONFIG/g935f.prop" ] && rm -f $CONFIG/g935f.prop
	[ -f "$CONFIG/g935fd.prop" ] && rm -f $CONFIG/g935fd.prop
	[ -f "$CONFIG/g935k.prop" ] && rm -f $CONFIG/g935k.prop
	[ -f "$CONFIG/g935l.prop" ] && rm -f $CONFIG/g935l.prop
	[ -f "$CONFIG/g935s.prop" ] && rm -f $CONFIG/g935s.prop
	[ -f "$CONFIG/g935w8.prop" ] && rm -f $CONFIG/g935w8.prop
	[ -f "$CONFIG/g935x.prop" ] && rm -f $CONFIG/g935x.prop
	for delete_prop in *.prop 
	do
		if grep "item" "$delete_prop"; then
			rm -f $delete_prop
		fi
		if grep "install=0" "$delete_prop"; then
			rm -f $delete_prop
		fi 
	done
	exit 10
fi

if [ $OPTION == "config_restore" ]; then
	## Restore Config
	# Copy backed up config files to $AROMA
	cp -f $CONFIG/* $AROMA
	exit 10
fi

if [ $OPTION == "wipe_magisk_su" ]; then
	## Wipe Magisk / SuperSU
	mount /cache
	rm -rf /system/.pin /system/bin/.ext /system/etc/.installed_su_daemon /system/etc/.has_su_daemon \
	/system/xbin/daemonsu /system/xbin/su /system/xbin/sugote /system/xbin/sugote-mksh /system/xbin/supolicy \
	/system/bin/app_process_init /system/bin/su /cache/su /system/lib/libsupol.so /system/lib64/libsupol.so \
	/system/su.d /system/etc/install-recovery.sh /system/etc/init.d/99SuperSUDaemon /cache/install-recovery.sh \
	/system/.supersu /cache/.supersu /data/.supersu \
	/system/app/Superuser.apk /system/app/SuperSU /cache/Superuser.apk \
	/cache/.supersu /cache/su.img /cache/SuperSU.apk \
	/data/.supersu /data/stock_boot_*.img.gz /data/su.img \
	/data/SuperSU.apk /data/app/eu.chainfire.supersu* \
	/data/data/eu.chainfire.supersu /data/supersu /supersu  2>/dev/null
	exit 10
fi

if [ $OPTION == "system_patch" ]; then
	## System Patches
	# Remove unwatned McRegistry entry
	rm -f /system/app/mcRegistry/ffffffffd00000000000000000000004.tlbin
	# Clean Apex data
	rm -rf /data/data/com.sec.android.app.apex
	# Remove init.d Placeholder
	rm -f /system/etc/init.d/placeholder
	# Delete Wakelock.sh 
	rm -f /magisk/phh/su.d/wakelock*
	rm -f /su/su.d/wakelock*
	rm -f /system/su.d/wakelock*
	rm -f /system/etc/init.d/wakelock*
	exit 10
fi

if [ $OPTION == "kernel_flash" ]; then
	## Flash Kernel (@dwander)
	# Clean up old kernels
	for i in $KERNEL_REMOVE; do
		if test -f $i; then
			[ -f $1 ] && rm -f $i
			[ -f sbin/$1 ] && rm -f sbin/$i
			sed -i "/$i/d" init.rc 
			sed -i "/$i/d" init.samsungexynos8890.rc 
		fi
		if test -f sbin/$i; then
			[ -f sbin/$1 ] && rm -f sbin/$i
			sed -i "/sbin\/$i/d" init.rc 
			sed -i "/sbin\/$i/d" init.samsungexynos8890.rc 
		fi
	done
	for i in $(ls ./res); do
		test $i != "images" && rm -R ./res/$i
	done
	[ -d /sbin/.backup ] && rm -rf /sbin/.backup
	[ -f /system/bin/uci ] && rm -f /system/bin/uci
	[ -f /system/xbin/uci ] && rm -f /system/xbin/uci
	# Flash new Image
	if grep -q install=1 $AROMA/g930x.prop; then
		dd if=$TGPTEMP/boot-s7.img of=$BLOCK/BOOT
	fi
	if grep -q install=1 $AROMA/g935x.prop; then
		dd if=$TGPTEMP/boot-s7e.img of=$BLOCK/BOOT
	fi
	sync
	exit 10
fi

if [ $OPTION == "splash_flash" ]; then
	## Custom Splash Screen (@Tkkg1994)
	cd /tmp/splash
	mkdir /tmp/splashtmp
	cd /tmp/splashtmp
	$AROMA/tar -xf $BLOCK/PARAM
	cp /tmp/splash/logo.jpg .
	chown root:root *
	chmod 444 logo.jpg
	touch *
	$AROMA/tar -pcvf ../new.tar *
	cd ..
	cat new.tar > $BLOCK/PARAM
	cd /
	rm -rf /tmp/splashtmp
	rm -f /tmp/new.tar
	sync
	exit 10
fi

if [ $OPTION == "adb" ]; then
	# Install ADB
	rm -f /system/xbin/adb /system/xbin/adb.bin /system/xbin/fastboot
	cp -f $AROMA/adb /system/xbin/adb
	cp -f $AROMA/adb.bin /system/xbin/adb.bin
	cp -f $AROMA/fastboot /system/xbin/fastboot
	chown 0:0 "/system/xbin/adb" "/system/xbin/adb.bin" "/system/xbin/fastboot"
	chmod 755 "/system/xbin/adb" "/system/xbin/adb.bin" "/system/xbin/fastboot"
	exit 10
fi

if [ $OPTION == "busybox" ]; then
	## Install Busybox
	rm -f /system/bin/busybox /system/xbin/busybox
	mv $AROMA/busybox /system/xbin/busybox
	chmod 0755 /system/xbin/busybox
	ln -s /system/xbin/busybox /system/bin/busybox
	/system/xbin/busybox --install -s /system/xbin
	exit 10
fi

if [ $OPTION == "backup_efs" ]; then	
	## Backup EFS
	mount $BLOCK/EFS /efs
	if [ ! -d /data/media/0/TGP ];then
	mkdir /data/media/0/TGP
	chmod -R 777 /data/media/0/TGP
	fi
	if [ -e /data/media/0/TGP/efs.img.bak ];then
	rm -f /data/media/0/TGP/efs.img.bak 
	fi
	if [ -e /data/media/0/TGP/efs.img ];then
	cp -f /data/media/0/TGP/efs.img /data/media/0/TGP/efs.img.bak
	rm -f /data/media/0/TGP/efs.img
	fi
	dd if=/dev/block/sda3 of=/data/media/0/TGP/efs.img bs=4096
	exit 10
fi

if [ $OPTION == "recovery_flash" ]; then
	## Flash Recovery
	if grep -q install=1 $AROMA/g930x.prop; then
		dd of=$BLOCK/RECOVERY if=$TGPTEMP/recovery-s7.img
	fi
	if grep -q install=1 $AROMA/g935x.prop; then
		dd of=$BLOCK/RECOVERY if=$TGPTEMP/recovery-s7e.img
	fi
	sync
	exit 10
fi

if [ $OPTION == "modem_flash" ]; then
	## Flash Modem
	cd $TGPTEMP
	cat modem.bin > /dev/block/sda8
	cat modem_debug.bin > /dev/block/sda17
	sync
	exit 10
fi

if [ $OPTION == "wipe" ]; then
	## Full Wipe
	find /data -maxdepth 1 -type d ! -path "/data" ! -path "/data/media" | xargs rm -rf
	exit 10
fi

if [ $OPTION == "variant_fix" ]; then
	## Variant Fix
	getprop ro.boot.bootloader >> /tmp/variant_model
	if grep -q G930 /tmp/variant_model; then
		sed -i -- 's/hero2lte/herolte/g' $BUILDPROP
		sed -i -- 's/3600 mAh/3000 mAh/g' $FLOATINGFEATURE
		sed -i -- 's/Galaxy S7 edge/Galaxy S7/g' $FLOATINGFEATURE
		sed -i -- 's/A3LSMG935F/A3LSMG930F/g' $FLOATINGFEATURE
		sed -i -- 's/HERO2_TYPE/HERO_TYPE/g' $FLOATINGFEATURE
	fi
	if grep -q G930F /tmp/variant_model; then
		sed -i -- 's/G935F/G930F/g' $BUILDPROP
	fi
	if grep -q G930FD /tmp/variant_model; then
		sed -i -- 's/G935F/G930FD/g' $BUILDPROP
	fi
	if grep -q G930K /tmp/variant_model; then
		sed -i -- 's/G935F/G930K/g' $BUILDPROP
	fi
	if grep -q G930L /tmp/variant_model; then
		sed -i -- 's/G935F/G930L/g' $BUILDPROP
	fi
	if grep -q G930S /tmp/variant_model; then
		sed -i -- 's/G935F/G930S/g' $BUILDPROP
	fi
	if grep -q G930W8 /tmp/variant_model; then
		sed -i -- 's/G935F/G930W8/g' $BUILDPROP
	fi
	if grep -q G935FD /tmp/variant_model; then
		sed -i -- 's/G935F/G935FD/g' $BUILDPROP
	fi
	if grep -q G935K /tmp/variant_model; then
		sed -i -- 's/G935F/G935K/g' $BUILDPROP
	fi
	if grep -q G935L /tmp/variant_model; then
		sed -i -- 's/G935F/G935L/g' $BUILDPROP
	fi
	if grep -q G935S /tmp/variant_model; then
		sed -i -- 's/G935F/G935S/g' $BUILDPROP
	fi
	if grep -q G935W8 /tmp/variant_model; then
		sed -i -- 's/G935F/G935W8/g' $BUILDPROP
	fi
	sed -i -- 's/ro.product.device=herolte/ro.product.device=hero2lte/g' $BUILDPROP
	sed -i -- 's/ro.build.product=herolte/ro.build.product=hero2lte/g' $BUILDPROP
	exit 10
fi

if [ $OPTION == "buildprop_mods" ]; then
	## build.prop Mods
	sed -i -- 's/ro.config.dmverity=true/ro.config.dmverity=false/g' $BUILDPROP
	echo "# Screen mirror fix" >> $BUILDPROP
	echo "wlan.wfd.hdcp=disable" >> $BUILDPROP
	echo "# Fix Vaultkeeper" >> $BUILDPROP
	echo "ro.security.vaultkeeper.feature=0" >> $BUILDPROP
	echo "# Fix SecureStorage" >> $BUILDPROP
	echo "ro.securestorage.knox=false" >> $BUILDPROP
	echo "ro.securestorage.support=false" >> $BUILDPROP
	echo "ro.security.mdpp.ux=Disabled" >> $BUILDPROP
	echo "# Fix Fingerprint Unlock" >> $BUILDPROP
	echo "fingerprint.unlock=1" >> $BUILDPROP
	echo "# Fix Mobile Hotspot" >> $BUILDPROP
	echo "net.tethering.noprovisioning=true" >> $BUILDPROP
	exit 10
fi

if [ $OPTION == "buildprop_tweaks" ]; then
	## build.prop Tweaks
	if grep -q install=1 $AROMA/tweaks-1.prop; then
		echo "# General Tweaks" >> $BUILDPROP
		echo "ro.media.enc.jpeg.quality=100" >> $BUILDPROP
		echo "persist.cne.feature=0" >> $BUILDPROP
		echo "ro.telephony.call_ring.delay=0" >> $BUILDPROP
		echo "ro.lge.proximity.delay=15" >> $BUILDPROP
		echo "mot.proximity.delay=15" >> $BUILDPROP
		echo "pm.sleep_mode=1" >> $BUILDPROP
		echo "ro.config.hw_fast_dormancy=1" >> $BUILDPROP
		echo "ro.config.vc_call_steps=20" >> $BUILDPROP
		echo "ro.ril.enable.amr.wideband=1" >> $BUILDPROP
		echo "ro.config.disable.hw_accel=false" >> $BUILDPROP
		echo "ro.fb.mode=1" >> $BUILDPROP
		echo "ro.sf.compbypass.enable=0" >> $BUILDPROP
		echo "persist.sys.ui.hw=1" >> $BUILDPROP
		echo "power.saving.mode=1" >> $BUILDPROP
		echo "ro.config.hw_quickpoweron=true" >> $BUILDPROP
		echo "ro.config.hw_power_saving=true" >> $BUILDPROP
		echo "ro.ril.disable.power.collapse=1" >> $BUILDPROP
		echo "wifi.supplicant_scan_interval=180" >> $BUILDPROP
		echo "debug.performance.tuning=1" >> $BUILDPROP
		echo "persist.sys.purgeable_assets=1" >> $BUILDPROP
		echo "ro.kernel.android.checkjni=0" >> $BUILDPROP
		echo "ro.kernel.checkjni=0" >> $BUILDPROP
	fi
	if grep -q install=1 $AROMA/tweaks-2.prop; then
		echo "# Animation Tweaks" >> $BUILDPROP
		echo "persist.sys.shutdown.mode=hibernate" >> $BUILDPROP
		echo "boot.fps=25" >> $BUILDPROP
		echo "shutdown.fps=25" >> $BUILDPROP
	fi
	if grep -q install=1 $AROMA/tweaks-3.prop; then
		echo "# Network Tweaks" >> $BUILDPROP
		echo "net.dns1=8.8.8.8" >> $BUILDPROP
		echo "net.dns2=8.8.4.4" >> $BUILDPROP
		echo "net.eth0.dns1=8.8.8.8" >> $BUILDPROP
		echo "net.eth0.dns2=8.8.4.4" >> $BUILDPROP
		echo "net.gprs.dns1=8.8.8.8" >> $BUILDPROP
		echo "net.gprs.dns2=8.8.4.4" >> $BUILDPROP
		echo "net.ipv4.ip_no_pmtu_disc=0" >> $BUILDPROP
		echo "net.ipv4.route.flush=1" >> $BUILDPROP
		echo "net.ipv4.tcp_ecn=0" >> $BUILDPROP
		echo "net.ipv4.tcp_fack=1" >> $BUILDPROP
		echo "net.ipv4.tcp_mem=187000" >> $BUILDPROP
		echo "net.ipv4.tcp_moderate_rcvbuf=1" >> $BUILDPROP
		echo "net.ipv4.tcp_no_metrics_save=1" >> $BUILDPROP
		echo "net.ipv4.tcp_rfc1337=1" >> $BUILDPROP
		echo "net.ipv4.tcp_rmem=4096" >> $BUILDPROP
		echo "net.ipv4.tcp_sack=1" >> $BUILDPROP
		echo "net.ipv4.tcp_timestamps=1" >> $BUILDPROP
		echo "net.ipv4.tcp_window_scaling=1" >> $BUILDPROP
		echo "net.ipv4.tcp_wmem=4096" >> $BUILDPROP
		echo "net.ppp0.dns1=8.8.8.8" >> $BUILDPROP
		echo "net.ppp0.dns2=8.8.4.4" >> $BUILDPROP
		echo "net.rmnet0.dns1=8.8.8.8" >> $BUILDPROP
		echo "net.rmnet0.dns2=8.8.4.4" >> $BUILDPROP
		echo "net.tcp.buffersize.default=4096,87380,256960,4096,16384,256960" >> $BUILDPROP
		echo "net.tcp.buffersize.edge=4096,87380,256960,4096,16384,256960" >> $BUILDPROP
		echo "net.tcp.buffersize.evdo_b=6144,87380,1048576,6144," >> $BUILDPROP
		echo "net.tcp.buffersize.gprs=4096,87380,256960,4096,16384,256960" >> $BUILDPROP
		echo "net.tcp.buffersize.hsdpa=6144,87380,1048576,6144,8" >> $BUILDPROP
		echo "net.tcp.buffersize.hspa=6144,87380,524288,6144,163" >> $BUILDPROP
		echo "net.tcp.buffersize.lte=524288,1048576,2097152,5242" >> $BUILDPROP
		echo "net.tcp.buffersize.umts=4096,87380,256960,4096,16384,256960" >> $BUILDPROP
		echo "net.tcp.buffersize.wifi=4096,87380,256960,4096,16384,256960" >> $BUILDPROP
		echo "net.wlan0.dns1=8.8.8.8" >> $BUILDPROP
		echo "net.wlan0.dns2=8.8.4.4" >> $BUILDPROP
		echo "# Signal Tweaks" >> $BUILDPROP
		echo "persist.cust.tel.eons=1" >> $BUILDPROP
		echo "ro.ril.enable.3g.prefix=1" >> $BUILDPROP
		echo "persist.telephony.support.ipv4=1" >> $BUILDPROP
		echo "persist.telephony.support.ipv6=1" >> $BUILDPROP
		echo "ro.ril.enable.dtm=1" >> $BUILDPROP
		echo "ro.ril.gprsclass=10" >> $BUILDPROP
		echo "ro.ril.hep=1" >> $BUILDPROP
		echo "ro.ril.hsdpa.category=10" >> $BUILDPROP
		echo "ro.ril.hsupa.category=5" >> $BUILDPROP
		echo "ro.ril.hsxpa=2" >> $BUILDPROP
		echo "ro.telephony.call_ring.delay=0" >> $BUILDPROP
	fi
	if grep -q install=1 $AROMA/tweaks-4.prop; then
		echo "# Multiuser Tweaks" >> $BUILDPROP
		echo "fw.max_users=10" >> $BUILDPROP
		echo "fw.show_multiuserui=1" >> $BUILDPROP
		echo "fw.show_hidden_users=1" >> $BUILDPROP
		echo "fw.power_user_switcher=1" >> $BUILDPROP
	fi
	exit 10
fi

if [ $OPTION == "apk_patch" ]; then
	## APK Patching Scripts
	# Patching Script
	FUNC_PATCH()
	{
	mkdir /tmp/work
	cp -f $APKLocation/$APKName/$APKName.apk /tmp/apk_patch/$APKName.zip
	cd $APKPatch
	$AROMA/zip -r -0 /tmp/apk_patch/$APKName.zip *
	rm -rf $APKLocation/$APKName/$APKName.apk
	cp -f /tmp/apk_patch/$APKName.zip $APKLocation/$APKName/$APKName.apk
	rm -rf /tmp/work
	}
	# Patching Script for framework-res
	FUNC_PATCH_FRAMEWORK()
	{
	mkdir /tmp/work
	cp -f $APKLocation/$APKName.apk /tmp/apk_patch/$APKName.zip
	cd $APKPatch
	$AROMA/zip -r -0 /tmp/apk_patch/$APKName.zip *
	rm -rf $APKLocation/$APKName/$APKName.apk
	cp -f /tmp/apk_patch/$APKName.zip $APKLocation/$APKName.apk
	rm -rf /tmp/work
	}
	# Patch APK Files (SystemUI)
	APKLocation=/system/priv-app
	APKName=SystemUI
	APKPatch=/tmp/apk_patch/SystemUI
	[ -d /tmp/apk_patch/SystemUI ] && FUNC_PATCH
	# Patch APK Files (framework.res)
	APKLocation=/system/framework
	APKName=framework-res
	APKPatch=/tmp/apk_patch/framework-res
	[ -d /tmp/apk_patch/framework-res ] && FUNC_PATCH_FRAMEWORK
	exit 10
fi

if [ $OPTION == "csc_tweaks" ]; then
	## CSC Tweaks
	# Camera
	find $CSC_DIRECTORY -type f -name "others.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_Camera_EnableCameraDuringCall>TRUE</CscFeature_Camera_EnableCameraDuringCall>' {} +
	find $CSC_DIRECTORY -type f -name "others.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_Camera_ShutterSoundMenu>TRUE</CscFeature_Camera_ShutterSoundMenu>' {} +
	# Air Message
	find $CSC_DIRECTORY -type f -name "others.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_Common_EnableAirMessage>FALSE</CscFeature_Common_EnableAirMessage>' {} +
	# Lockscreen
	find $CSC_DIRECTORY -type f -name "others.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_LockScreen_EnableOperatorWallpaper>TRUE</CscFeature_LockScreen_EnableOperatorWallpaper>' {} +
	# Message
	find $CSC_DIRECTORY -type f -name "others.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_Message_EnableSaveRestoreSDCard>TRUE</CscFeature_Message_EnableSaveRestoreSDCard>' {} +
	find $CSC_DIRECTORY -type f -name "others.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_Message_EnableSaveVMessage>TRUE</CscFeature_Message_EnableSaveVMessage>' {} +
	find $CSC_DIRECTORY -type f -name "others.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_Message_EnableMsgTypeIndicationDuringComposing>TRUE</CscFeature_Message_EnableMsgTypeIndicationDuringComposing>' {} +
	find $CSC_DIRECTORY -type f -name "others.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_Message_DisableSmsToMmsConversionByTextInput>TRUE</CscFeature_Message_DisableSmsToMmsConversionByTextInput>' {} +
	find $CSC_DIRECTORY -type f -name "others.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_Message_AddSendOptionInComposer>TRUE</CscFeature_Message_AddSendOptionInComposer>' {} +
	find $CSC_DIRECTORY -type f -name "others.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_Message_EnableMenuMmsDeliveryTime>TRUE</CscFeature_Message_EnableMenuMmsDeliveryTime>' {} +
	find $CSC_DIRECTORY -type f -name "others.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_Message_EnableSMSPcheckWhenSendSMS>TRUE</CscFeature_Message_EnableSMSPcheckWhenSendSMS>' {} +
	find $CSC_DIRECTORY -type f -name "others.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_Message_ImageResizeQualityFactorWhenAttach>TRUE</CscFeature_Message_ImageResizeQualityFactorWhenAttach>' {} +
	find $CSC_DIRECTORY -type f -name "others.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_Message_EnableSpamFilteringEngine>TRUE</CscFeature_Message_EnableSpamFilteringEngine>' {} +
	# RIL
	find $CSC_DIRECTORY -type f -name "others.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_RIL_ForceConnectMMS>TRUE</CscFeature_RIL_ForceConnectMMS>' {} +
	# Setting
	find $CSC_DIRECTORY -type f -name "others.xml" -exec sed -i -- '/CscFeature_Setting_EnableMenuBlockCallMsg/d' {} +
	find $CSC_DIRECTORY -type f -name "others.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_Setting_SupportRealTimeNetworkSpeed>TRUE</CscFeature_Setting_SupportRealTimeNetworkSpeed>' {} +
	find $CSC_DIRECTORY -type f -name "others.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_Setting_CustNetworkSelMenu4>LTEONLY</CscFeature_Setting_CustNetworkSelMenu4>' {} +
	find $CSC_DIRECTORY -type f -name "others.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_Setting_SupportDataUsageReminder>TRUE</CscFeature_Setting_SupportDataUsageReminder>' {} +
	find $CSC_DIRECTORY -type f -name "others.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_Setting_EnableMenuBlockCallMsg>TRUE</CscFeature_Setting_EnableMenuBlockCallMsg>' {} +
	# SmartManager
	find $CSC_DIRECTORY -type f -name "others.xml" -exec sed -i -- '/CscFeature_SmartManager_ConfigSubFeatures/d' {} +
	find $CSC_DIRECTORY -type f -name "others.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_SmartManager_ConfigDashboard>dual_dashboard</CscFeature_SmartManager_ConfigDashboard>' {} +
	find $CSC_DIRECTORY -type f -name "others.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_SmartManager_ConfigSubFeatures>roguepopup|autoclean|autorestart|storageclean|backgroundapp|applock|data_compression|cstyle|autolaunch|appclean|fake_base_station|notificationmanager|UDS2</CscFeature_SmartManager_ConfigSubFeatures>' {} +
	find $CSC_DIRECTORY -type f -name "others.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_SmartManager_DisableAntiMalware>TRUE</CscFeature_SmartManager_DisableAntiMalware>' {} +
	find $CSC_DIRECTORY -type f -name "others.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_SmartManager_ConfigDashboard>dual_dashboard</CscFeature_SmartManager_ConfigDashboard>' {} +
	# SystemUI
	find $CSC_DIRECTORY -type f -name "others.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_SystemUI_SupportAssistanceAppChooser>TRUE</CscFeature_SystemUI_SupportAssistanceAppChooser>' {} +
	find $CSC_DIRECTORY -type f -name "others.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_SystemUI_SupportRecentAppProtection>TRUE</CscFeature_SystemUI_SupportRecentAppProtection>' {} +
	find $CSC_DIRECTORY -type f -name "others.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_SystemUI_ConfigOpBrandingForQuickSettingLabel>CHC</CscFeature_SystemUI_ConfigOpBrandingForQuickSettingLabel>' {} +
	find $CSC_DIRECTORY -type f -name "others.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_SystemUI_ConfigOpBrandingQuickSettingIcon>CHC</CscFeature_SystemUI_ConfigOpBrandingQuickSettingIcon>' {} +
	find $CSC_DIRECTORY -type f -name "others.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_SystemUI_ConfigQuickSettingPopup>CHC</CscFeature_SystemUI_ConfigQuickSettingPopup>' {} +
	find $CSC_DIRECTORY -type f -name "others.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_SystemUI_ConfigDefQuickSettingItem>Wifi,SilentMode,Bluetooth,RotationLock,Flashlight,AirplaneMode,PowerSaving,MobileData,WifiCalling,Performance,BlueLightFilter,WifiHotspot,PersonalMode,SecureFolder,Location,Nfc,Aod,AllShareCast,DeviceVisibility,Dnd,Sync,UDS,BikeMode,PowerPlanning,EdgeLighting,FloatingMessage,DormantMode,NetworkBooster,QuickConnect,SmartStay,SmartPause,AirView,AirBrowse,Toolbox,CarMode,UltraPowerSaving,SFinder,ScreenCapture,UHQ,VoLte,Dolby</CscFeature_SystemUI_ConfigDefQuickSettingItem>' {} +
	# VoiceCall
	find $CSC_DIRECTORY -type f -name "others.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_VoiceCall_ConfigRecording>RecordingAllowed</CscFeature_VoiceCall_ConfigRecording>' {} +
	# Wifi
	find $CSC_DIRECTORY -type f -name "others.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_Wifi_SupportMobileApOnTrigger>TRUE</CscFeature_Wifi_SupportMobileApOnTrigger>' {} +
	find $CSC_DIRECTORY -type f -name "others.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_Wifi_SupportAdvancedMenu>TRUE</CscFeature_Wifi_SupportAdvancedMenu>' {} +
	exit 10
fi

if [ $OPTION == "csc_detect" ]; then
	## Script by ALEXNDR to detect CSC automatically and edit suitable entry in csc.prop as needed
	if [ -f /efs/imei/mps_code.dat ] ; then ACT_CSC=`cat /efs/imei/mps_code.dat` ; else ACT_CSC=FAIL ; fi
	if [ -f /system/csc/sales_code.dat ] ; then FW_CSC=`cat /system/csc/sales_code.dat` ; else FW_CSC=FAIL ; fi
	if [ ! -z "`cat /tmp/SW_Configuration.xml | grep "/"$ACT_CSC"/"`" ] ; then
		sed -i '/Skip/d' /tmp/aroma/csc.prop
		echo "csc="$ACT_CSC"" >> /tmp/aroma/csc.prop
	elif [ ! -z "`cat /tmp/SW_Configuration.xml | grep "/"$FW_CSC"/"`" ] ; then
		sed -i '/Skip/d' /tmp/aroma/csc.prop
		echo "csc="$FW_CSC"" >> /tmp/aroma/csc.prop
	elif [ ! -f /system/csc/sales_code.dat ] ; then
		sed -i '/Skip/d' /tmp/aroma/csc.prop
		echo "csc=BTU" >> /tmp/aroma/csc.prop
	fi
	exit 10
fi

if [ $OPTION == "csc_backup" ]; then
	## Backup CSC Script to keep original CSC pack (@ALEXNDR)
	mkdir -p /tmp/kcsc/system/etc
	cp -R /system/csc /tmp/kcsc/system
	cp -R /system/VODB /tmp/kcsc/system
	cp -R /system/sipdb /tmp/kcsc/system
	cp -R /system/finder_cp /tmp/kcsc/system
	cp -R /system/wallpaper /tmp/kcsc/system
	cp -R /system/HWRDB /tmp/kcsc/system
	cp -R /system/T9DB /tmp/kcsc/system
	cp /system/CSCVersion.txt /tmp/kcsc/system
	cp /system/SW_Configuration.xml /tmp/kcsc/system
	cp /system/etc/csc_*.txt /tmp/kcsc/system/etc
	cp /system/etc/enforce*.txt /tmp/kcsc/system/etc
	cp /system/etc/hidden_*.txt /tmp/kcsc/system/etc
	cp /system/etc/*keystrings.dat /tmp/kcsc/system/etc
	cp /system/etc/lteon_netlist.xml /tmp/kcsc/system/etc
	cp /system/etc/plmn_delta.bin /tmp/kcsc/system/etc
	if [ -h /system/csc_contents ] ; then
		readlink /system/csc_contents > /tmp/kcsc/csc_contents
	fi
	exit 10
fi

if [ $OPTION == "csc_restore" ]; then
	## Restore CSC Script to keep original CSC pack (@ALEXNDR)
	cp -fR /tmp/kcsc/system/* /system
	if [ -f /system/csc/feature.bak ] ; then
		mv /system/csc/feature.bak /system/csc/feature.xml
	fi
	if [ -f /system/csc/others.bak ] ; then
		mv /system/csc/others.bak /system/csc/others.xml
	fi
	CURRENT_CSC=`cat /system/csc/sales_code.dat`
	if [ -f /system/csc/feature.xml ] && [ -h /system/csc_contents ] ; then
		cp -f /system/csc/feature.xml /system/csc/$CURRENT_CSC/system/csc/feature.xml
	elif [ -f /system/csc/others.xml ] && [ -h /system/csc_contents ] ; then
		cp -f /system/csc/others.xml /system/csc/$CURRENT_CSC/system/csc/others.xml
	fi
	if [ -f /tmp/kcsc/csc_contents ] ; then
	ln -s `cat /tmp/kcsc/csc_contents` /system/csc_contents
	fi
	rm -fR /tmp/kcsc
	exit 10
fi

if [ $OPTION == "set_csc" ]; then
	## Install Selected CSC Script (@ALEXNDR)
	rm -f /system/csc/*.*
	rm -f /system/csc_contents
	rm -f /system/etc/csc_*.txt
	rm -f /system/etc/hidden_*.txt
	cp -fR /system/csc/common/system/* /system
	cp -fR /system/csc/$CSC/system/* /system
	ln -sf /system/csc/$CSC/csc_contents /system/csc_contents
	exit 10
fi

if [ $OPTION == "csc_fix" ]; then
	## CSC Fixes (@Tkkg1994)
	sed -i -- "s/CSC=//g" /tmp/aroma/csc.prop
	getprop ro.boot.bootloader >> /tmp/variant
	NEW_CSC=`cat /tmp/aroma/csc.prop`
	ACTUAL_CSC=`cat /efs/imei/mps_code.dat`
	if grep -q G935 /tmp/variant;
	then
		sed -i -- "s/G930/G935/g" /system/CSCVersion.txt
		sed -i -- "s/G930/G935/g" /system/SW_Configuration.xml
	else if grep -q G930 /tmp/variant;
	then
		sed -i -- "s/G935/G930/g" /system/CSCVersion.txt
		sed -i -- "s/G935/G930/g" /system/SW_Configuration.xml
	fi
	fi
	if [ -f "/tmp/aroma/csc.prop" ]; then
		if [ "$NEW_CSC" == "TMB" ]; then
			sed -i -- "s/$ACTUAL_CSC/ZTO/g" /efs/imei/mps_code.dat
		else if [ "$NEW_CSC" == "ATT" ]; then
			sed -i -- "s/$ACTUAL_CSC/ZTO/g" /efs/imei/mps_code.dat
		else if [ "$NEW_CSC" == "VZW" ]; then
			sed -i -- "s/$ACTUAL_CSC/ZTO/g" /efs/imei/mps_code.dat
		else if [ "$NEW_CSC" == "XAS" ]; then
			sed -i -- "s/$ACTUAL_CSC/ZTO/g" /efs/imei/mps_code.dat
		else
			sed -i -- "s/$ACTUAL_CSC/$NEW_CSC/g" /efs/imei/mps_code.dat
		fi
		fi
		fi
		fi
	fi
	exit 10
fi

