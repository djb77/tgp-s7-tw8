#!/bin/bash
# TGP Build Script for Linux v1.22 by djb77 / XDA-Developers

export rootdir=$(pwd)
export tgpdir=$rootdir/build/tgp
export tgpfirmware=$(<tools/firmware)
export tgpversion=$(<tools/version)
export zipname='TGP_G93xx_'$tgpfirmware'_v'$tgpversion'.zip'

echo ""
echo "TGP build script by @djb77"
echo "--------------------------"
echo ""
echo "Building Zip File $zipname"
cd $rootdir/build
zip -9gq $zipname -r META-INF/ -x "*~"
zip -9gq $zipname -r tgp/ -x "*~"
mv $zipname $rootdir/$zipname
cd $rootdir
chmod a+r $zipname

echo ""
echo "Done."
echo ""

